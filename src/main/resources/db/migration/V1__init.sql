DROP TABLE IF EXISTS `t_examples`;
CREATE TABLE `t_examples` (
  id         VARCHAR(32) PRIMARY KEY,
  name       VARCHAR(10),
  type       VARCHAR(10),
  active     VARCHAR(10),
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NULL,
  deleted_at TIMESTAMP NULL
);

