package in.hocg.web.domain.enums;

import com.baomidou.mybatisplus.enums.IEnum;

import java.io.Serializable;

public enum ExampleType implements IEnum {
  AliPay,
  WeiXin;

  @Override
  public Serializable getValue() {
    return this.name();
  }
}
