package in.hocg.web.mapper;

import in.hocg.web.domain.Example;
import in.hocg.web.utils.support.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ExampleMapper extends SuperMapper<Example> {

  Collection<Example> findLike(String name);

  Collection<Example> findByIDs(@Param("IDs") String... IDs);

}
