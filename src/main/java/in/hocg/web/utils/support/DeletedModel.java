package in.hocg.web.utils.support;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class DeletedModel<T extends Model> extends DefaultModel<T> {
  @TableField(value = "deleted_at")
  private Timestamp deletedAt;
  /**
   * true 为未删除
   * false 为删除
   */
  @TableField(value = "active")
  @TableLogic
  private String active;
}
