package in.hocg.web.utils.support;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by hocgin on 2018/6/12.
 * email: hocgin@gmail.com
 */
public abstract class BaseController {
    @GetMapping("{page:.*?\\.html$}")
    public String index(@PathVariable("page") String page) {
        return page(page);
    }
    
    protected abstract String page(String page);
}
