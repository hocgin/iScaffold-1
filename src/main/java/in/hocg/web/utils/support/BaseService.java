package in.hocg.web.utils.support;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * ExampleMapper, Example
 * @param <M> Mapper 接口
 * @param <T> 对象实体
 * - 增加 extends DefaultModal -> set created_at
 * - 删除 extends DeletedModal -> set deleted_at
 * - 查询 extends DeletedModal -> deleted_at == null
 *
 */
@Transactional
public abstract class BaseService<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {
}
