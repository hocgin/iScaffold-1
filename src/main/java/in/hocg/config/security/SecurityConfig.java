package in.hocg.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by hocgin on 2018/6/13.
 * email: hocgin@gmail.com
 */
@Component
public class SecurityConfig {
    static String ROLE_SELLER = "ROLE_SELLER";
    static String SIGN_IN = "/admin/sign-in.html";
    static String SIGN_UP = "/admin/sign-up.html";
    
    
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
