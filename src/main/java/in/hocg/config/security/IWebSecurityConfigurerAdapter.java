package in.hocg.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by hocgin on 2018/6/13.
 * email: hocgin@gmail.com
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class IWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
    
    @Qualifier("IUserDetailsService")
    @Autowired
    private UserDetailsService userDetailsService;
    
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("/admin/**")
                .userDetailsService(userDetailsService)
                .csrf().and().headers().frameOptions().disable().and()
        
                // 静态资源
                .authorizeRequests()
                .antMatchers(HttpMethod.GET
                        ,"/**/*.js"
                        ,"/**/*.css"
                        ,"/**/*.woff"
                        ,"/**/*.png"
                        ,"/**/*.gif"
                        ,"/**/*.jpg"
                ).permitAll()
                // 页面
                .antMatchers(SecurityConfig.SIGN_IN,
                        SecurityConfig.SIGN_UP,
                        "/admin/forget.html"
                ).permitAll()
                
                .anyRequest()
                .authenticated().and()
                .formLogin()
                .loginProcessingUrl("/admin/login")
                .loginPage(SecurityConfig.SIGN_IN)
                .failureUrl(String.format("%s?error=true", SecurityConfig.SIGN_IN))
                .defaultSuccessUrl("/admin/index.html", false)
                .permitAll().and()
                
                .rememberMe()
                .rememberMeParameter("remember")
                .and()
                
                .logout()
                .logoutUrl("/admin/logout")
                .permitAll()
        
        ;
    }
}
